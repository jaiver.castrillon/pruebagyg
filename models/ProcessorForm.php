<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Validations;

/**
 * ProcessorForm is responsible for calculating the functions.
 */
class ProcessorForm extends Model
{   
    /**
     * @var array[] $a the array A, defined in the challenge.
     * @var array[] $z the array that contains the segments with the ranges.
     * @var array[] $solution this array contains the solution of each segment.
     * @var array[] $tabla_dinamica, this array helps the algorithm optimization through dynamic programming.
     */
    public $a;
    public $z;
    public $solution = [];
    public $tabla_dinamica = [];
    public $time_exec;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['a', 'z'], 'required']
        ];
    }


    /**
     * Function responsible of calculate principal of challenge.
     */
    public function calculate(){
        $time_init = microtime(true);
        foreach ($this->z as $segment){
            $sum = 0;
            for ($l = $segment[0]-1; $l < $segment[1]; $l +=1){
                for ($r = $l; $r < $segment[1]; $r += 1){                   
                    if(!isset($this->tabla_dinamica[$l][$r])){
                        $this->tabla_dinamica[$l][$r] = $this->maximInRange($this->a, $l, $r);;  
                    }
                    //$sum += $this->maximInRange($this->a, $l, $r);
                    $sum += $this->tabla_dinamica[$l][$r];

                }
            }
            array_push($this->solution, $sum);
        }
        $time_end = microtime(true);
        $this->time_exec = round($time_end - $time_init, 4);
    }

    /**
     * This function obtains the maximun number in a range of an array. 
     * @param array[] $arr, the array where to find maximun. 
     * @param int $min, lower limit of the range.
     * @param int $max, upper limit of the range.
     * @return int the maximun numer in the range defined.
     */
    public function maximInRange($arr, $min, $max){
        if(count($arr) == 1){
            return $arr[$min];
        }
        return max(array_slice($arr, $min, ($max + 1) - ($min)));
    }


    /**
     * Validates the arrays entered by user.
     * @return array[] array of two positions, the first contains a bool indicating if
     * are correct the arrays, and the second position contains a string that is the message
     * for the user.
     */
    public function validateArrays(){
        $validations = new Validations;

        $this->a = explode(" ", $this->a);   
        $this->z = explode("\n", $this->z);
        
        for ($i = 0; $i < count($this->z); $i += 1) {
            $this->z[$i] = explode(" ", $this->z[$i]);  
            for ($x = 0; $x < count($this->z[$i]); $x += 1) {
                $this->z[$i][$x] = trim($this->z[$i][$x]);
            }
        }
        if($validations->validateNumbers($this->a)){
            for ($i = 0; $i < count($this->a); $i += 1) {
                $this->a[$i] = (int)($this->a[$i]); 
            }
        }else{
            return [false, "Error en A, deben ser números."];    
        }

        if($validations->validateNumbersSegments($this->z)){      
            return $validations->validateSegments($this->z, count($this->a));             
        }else{
            return [false, "Error en segmentos, deben ser números."];    
        }
    }

}
