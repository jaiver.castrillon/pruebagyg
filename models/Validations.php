<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Model for validate the data entry.
 */
class Validations extends Model
{

    /**
     * Validate that the array contains only digits numerics.
     * @param array[] $vec array to analyze.
     * @return bool indicate if contains digits numerics or no.
     */
    public function validateNumbers($vec)
    {   
        foreach ($vec as $key) {
            if (!is_numeric(trim($key))){
                return false;
            }    
        }
        return true;
    }

    /**
     * Validate that the array of arrays contains only digits numerics.
     * @param array[] $vec array to analyze.
     * @return bool indicate if contains digits numerics or no.
     */
    public function validateNumbersSegments($vec){

        foreach ($vec as $key) {
            if (!$this->validateNumbers($key)){
                return false;
            }    
        }
        return true;
    }

    /**
     * Validate that each segment has two positions, and limit is less that 
     * length of array A.
     * @param array[] $vec array to analyze.
     * @param int $value length of array A.
     * @return array[] array of two positions, the first contains a bool indicating if
     * is correct array, and the second position contains a string that is the message
     * for the user.
     */
    public function validateSegments($vec, $value){
        foreach ($vec as $key) {
            
            if (count($key) != 2){
                return [false, "Sólo se aceptan dos números para cada segmento."];
            }else{
                if($key[0] > $key[1]){
                    return [false, "El primer valor debe ser menor al segundo, en cada segmento."];
                }else{
                    if(!$this->validateSegmentValue($key[1], $value)){
                       return [false, "Los límites no pueden sobrepasar el tamaño del array A."];     
                    }
                }
            }   
        }
        return [true, ""];
    }
    
    /**
     * Validate that the limit higher of segment is less that length of array A.
     * @param int $seg value to analyze.
     * @param int $value length of array A.
     * @return bool indicate if $seg is less or higher that $value.
     */
    public function validateSegmentValue($seg, $value){
        return $seg <= $value;
    }
}
