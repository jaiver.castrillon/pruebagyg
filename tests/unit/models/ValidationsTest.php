<?php 

use app\models\Validations;

class ValidationsTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    private $validations;
    
    /**
     * Before of test must inicialized the object.
     */
    protected function _before(){
        $this->validations = new Validations;
    }

    protected function _after(){
    }

    /**
     * Check that the method validate the size of each segment, the value of range,
     * the value of each segment according the size of main array and obviously his functioning.
     */
    public function testValidateSegments(){
        $arr_temp = [["1","2"],["2","3"]];
        $this->assertEquals($this->validations->validateSegments($arr_temp, 3), [true,""]);
        $arr_temp = [["1","2"],["2"]];
        $this->assertEquals($this->validations->validateSegments($arr_temp, 3), [false, "Sólo se aceptan dos números para cada segmento."]);
        $arr_temp = [["2","1"],["2","3"]];
        $this->assertEquals($this->validations->validateSegments($arr_temp, 3), [false, "El primer valor debe ser menor al segundo, en cada segmento."]);
        $arr_temp = [["1","2"],["2","3"]];
        $this->assertEquals($this->validations->validateSegments($arr_temp, 2), [false, "Los límites no pueden sobrepasar el tamaño del array A."]);
    }

    /**
     * Verify that the user's input array 'A' is numbers, checking with strings such as entries,
     * negative numbers, and entering letters in the strings
     */
    public function testValidateNumbers(){
        $arr_temp = ["12", 2, " 32 ", "-1323"];
        $this->assertTrue($this->validations->validateNumbers($arr_temp));
        $arr_temp = ["AA12", true, " ffd32 ", "-1323"];
        $this->assertFalse($this->validations->validateNumbers($arr_temp));
    }

    /**
     * Verify that the user's input array 'segments' is numbers, checking with strings such as entries,
     * negative numbers, and entering letters in the strings
     */
    public function testValidateNumbersSegments(){
        $arr_temp = [["23", "2"],[345, "   2"]];
        $this->assertTrue($this->validations->ValidateNumbersSegments($arr_temp));
        $arr_temp = [["DD"],["12"]];
        $this->assertFalse($this->validations->validateNumbersSegments($arr_temp));
    }

    /**
     * Check the limits of correct range.
     */
    public function testValidateSegmentValue(){
        $this->assertTrue($this->validations->validateSegmentValue(1,4));
        $this->assertTrue($this->validations->validateSegmentValue(4,4));
        $this->assertFalse($this->validations->validateSegmentValue(5,4));
    }
}