<?php 

use app\models\ProcessorForm;

class ProcessorFormTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    private $processor;
    
    /**
     * Before of test must inicialized the object.
     */
    protected function _before(){
        $this->processor = new ProcessorForm;
        
    }

    protected function _after(){
    }

    /**
     * Check diferents combinations of ranges to find the maximun correct in each of them.
     */
    public function testMaximInRange(){
        $arr_temp = [1,3,2,-2];
        $this->assertEquals($this->processor->maximInRange($arr_temp, 0, 1), 3);
        $this->assertEquals($this->processor->maximInRange($arr_temp, 0, 2), 3);
        $this->assertEquals($this->processor->maximInRange($arr_temp, 1, 2), 3);
        $this->assertEquals($this->processor->maximInRange($arr_temp, 3, 3), -2);
        $this->assertEquals($this->processor->maximInRange($arr_temp, 2, 3), 2);
    }
}