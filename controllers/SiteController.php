<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\ProcessorForm;
use app\models\Validations;

/**
* Controller principal for communicate between the model Processor and
* views of entry´s information and results finals.
 */
class SiteController extends Controller
{
    /**
     * Main action where information is entered and validated to calculate later.
     * @return string rendering for the indicated view
     */
    public function actionIndex()
    {
        ini_set('memory_limit', '-1');
        $model = new ProcessorForm;

        if ($model->load(Yii::$app->request->post())) {
            $temp = $model->validateArrays(); 
            if ($temp[0]){
                $model->calculate();
                return $this->render('data', ['model' => $model]);
            }else{
                echo'<script type="text/javascript">
                alert("'.$temp[1].'");
                </script>';
                return $this->render('index', ['model' => new ProcessorForm]);
            }
            
        } else {
            return $this->render('index', ['model' => $model]);
        }
    }
}
