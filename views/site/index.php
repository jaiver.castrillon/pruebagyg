<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->title = 'Prueba';
?>

<!--Form for the entry of arrays to calculate.-->
<div class="site-index">
    <div class="body-content col-xs-5">
    <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'a')->label('Array A'); ?>
        <?= $form->field($model, 'z')->textarea()->label('Segmentos');  ?>

        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>        
    </div>
</div>
