<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->title = 'My Yii Application';
?>
<!--Temporary form to visualize the matrices entered and the final result .-->
<div class="site-index">
    <div class="body-content col-xs-4">
    
    <table class="table table-bordered table-dark">
        <thead>
            <tr>
                <th scope="col" colspan="3" >Array A</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="3">
                    <?php for($i = 0; $i < count($model->a); $i += 1)
                        echo $model->a[$i]."  ";
                    ?>
                </td>
            </tr>
            <tr>
                <th scope="col" colspan="2">Segmentos</th>
                <th scope="col">Solución</th>
            </tr>
            <?php for($i = 0; $i < count($model->z); $i += 1){
                    echo '<tr>';
                    foreach($model->z[$i] as $val){
                        echo '<td>'.$val.'</td>';
                    }
                    echo '<td>'.$model->solution[$i].'</td>';
                    echo '</tr>';
                }
            ?>
        </tbody>

    </table>
    <?php
        echo "<h4>Tiempo de ejecución: ".$model->time_exec." seg</h4>";
    ?>
    <?= Html::a('Regresar', ['site/index'], ['class' => 'btn btn-primary']) ?>
    </div>
</div>
